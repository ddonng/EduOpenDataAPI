var forgotAppModule = angular.module('forgotApp',[]);
forgotAppModule.config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    });
forgotAppModule.controller("forgotController",function($scope,$http){
	$scope.teacher={
		"phoneNumber":"",
		"tname":"",
		"captcha":""
	};
	$scope.stat={
		"phoneStat":"",
		'code':'',
		'msg':''
	};
	$scope.checkPhone = function(){

		var pattarn = /^0?(13[0-9]|15[0-9]|17[678]|18[0-9]|14[57])[0-9]{8}$/;
		if(pattarn.test($scope.teacher.phoneNumber))
		{
			$scope.stat.phoneStat='valid';

		}else{
			$scope.stat.phoneStat='invalid';
		}
	}

	$scope.resetPwd =function(){
		$http({
			url:'/account/repwd',
			method:'post',
			data:JSON.stringify({'teacher':$scope.teacher}),
			headers: {'Content-Type': 'application/json'}
		}).success(function(data,status,headers,config){
			// console.log(data);
			// console.log(status);
			// console.log(headers);
			// console.log(config);
			$scope.stat.code=data.code;
			$scope.stat.msg=data.message;

		}).error(function(data,status,headers,config){
			console.log("reset pwd post error!");
			console.log(data);
			console.log(status);
			console.log(headers);
			console.log(config);
		});
	}

});

angular.bootstrap(angular.element("#forgot-box"),["forgotApp"]);