var myAppModule = angular.module('myApp', [
	'mgcrea.ngStrap.popover'
	]);
myAppModule.config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    });
myAppModule.controller('registerController',['$scope','$http','$interval',function($scope,$http,$interval){
	$scope.popover = {
		"title":"提示",
		"content":"发送验证码"
	};
	$scope.addstat = {
		"code" : 0,
		"msg" : ""
	}
	$scope.user =Object();
	$scope.checkPhone = function(){
		
		var pattarn = /^0?(13[0-9]|15[0-9]|17[678]|18[0-9]|14[57])[0-9]{8}$/;
		if(pattarn.test($scope.user.phoneNumber))
		{
			$scope.stat='checked';

		}else{
			$scope.stat='unchecked';
		}
		$scope.$broadcast('manual');
	}
	var num = 60;
	$scope.counter = num;
	var timer;
	$scope.sms = function(){
		$http.get('/account/captcha',{params:{phoneNumber:$scope.user.phoneNumber}}).success(function(data,status,headers,config){
				// console.log(data);
				console.log(data.code);
				if(data.code){
					if(data.code==='-1')
					{
						$scope.stat='exists';
					}else{
						$scope.stat='success';
					    timer = $interval(function(){
					    	if($scope.counter>0)
					        	$scope.counter--;
					        if($scope.counter==0)
					        {
					        	$scope.stat='checked';
					        	$scope.counter=num;
					        }
					        // console.log($scope.counter);
					        // $scope.$apply();
				    	}, 1000,$scope.counter);
					}

				}else{
					$scope.stat='fail';
				}

		});
	}
	$scope.checkPwd = function(){
			if ($scope.user.passwd.length < 8){
				$scope.pwdstat = 'short';
			}else{
				$scope.pwdstat = 'good';
			}
                  
	}
	$scope.checkEmail = function(){
		var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if(regex.test($scope.user.email))
		{
			$scope.emailstat = 'valid';
		}else{
			$scope.emailstat = 'invalid';
		}
	}

	$scope.checkDept = function(){
		// 每次清空
		$scope.user.department=undefined;
	}

	$scope.checkOfficePhone =function(){
		var regex = /^([0-9]{3,4}-)?[0-9]{7,8}$/;
		if( $scope.user.officePhone=="" || regex.test($scope.user.officePhone))
		{
			// console.log($scope.user.officePhone);
			$scope.ophstat = 'valid';
		}else{
			$scope.ophstat = 'invalid';
		}
	}

	$scope.$watchCollection('user',function(newUser, oldUser,scope){
		if(angular.isUndefined($scope.user.phoneNumber)|| $scope.stat=='unchecked' || angular.isUndefined($scope.user.captcha) ||angular.isUndefined($scope.user.passwd) || $scope.pwdstat =='short')
		{
			// console.log($scope.user.phoneNumber);
			$scope.canext="no";
		}else{

			$scope.canext="yes";
		}

		// 检查user
		if($scope.stat=='unchecked' || $scope.emailstat == 'invalid' || angular.isUndefined($scope.user.sex) || angular.isUndefined($scope.user.ptitle) || angular.isUndefined($scope.user.isInsideSchool) || $scope.ophstat=='invalid' ||angular.isUndefined($scope.user.department))
		{
			$scope.canreg='no';
		}else{
			$scope.canreg='yes';
		}

	});

	$scope.addUser =function(){
		// console.log($scope.user);
		$http({
			url:'/account/addUser',
			method:'post',
			data:JSON.stringify({'user':$scope.user}),
			headers: {'Content-Type': 'application/json'}
		}).success(function(data,status,headers,config){

				// console.log(data);
				// console.log(status);
				// console.log(headers);
				// console.log(config);
				$scope.addstat.code=data.code;
				$scope.addstat.msg=data.message;

			}).error(function(data,status,headers,config){
				console.log('AddUser post error');
				// console.log(data);
				// console.log(status);
				// console.log(headers);
				// console.log(config);
		});
	}


}]);