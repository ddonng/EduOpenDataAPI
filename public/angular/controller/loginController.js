var myAppModule = angular.module('myapp', ['mgcrea.ngStrap']);
myAppModule.config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    });
myAppModule.controller('loginController',function($scope,$http){
	$scope.tooltip = {
	  "title": "若忘记账号请咨询项目科",
	  "checked": false
	};
	$scope.user =Object();
	$scope.stat =Object();
	function validateUserID(){
		var phonePattarn = /^0?(13[0-9]|15[0-9]|17[678]|18[0-9]|14[57])[0-9]{8}$/;
			var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return (phonePattarn.test($scope.user.userid) || emailRegex.test($scope.user.userid));

	}

	$scope.checkUserID = function(){
	
		if(validateUserID()){
			$scope.stat.lstat="valid";
		}else{
			$scope.stat.lstat="invalid";
			$scope.stat.lmsg = "邮箱或手机号码输入有误，请仔细检查";
		}
	}

	$scope.changePwd = function(){
		if($scope.user.pwd!==null && !angular.isUndefined($scope.user.pwd) && $scope.user.pwd.length>4)
		{
			$scope.stat.pstat = "full";
		}

	}
	$scope.blurPwd = function(){
		if($scope.user.pwd===null || angular.isUndefined($scope.user.pwd) || $scope.user.pwd.length<5)
		{
			$scope.stat.pstat = "empty";
		}else{
			$scope.stat.pstat = "full";
		}

	}

	$scope.$watchCollection('stat',function(newUser,oldUser,scope){
		if($scope.stat.lstat=="invalid" || $scope.stat.pstat=="empty")
		{
			$("#loginbt").attr('disabled','disabled');
			
		}
		if($scope.stat.pstat=="full" && ($scope.stat.lstat=="valid" || validateUserID()))
		{
			$("#loginbt").removeAttr('disabled');
		}
		
	});
});