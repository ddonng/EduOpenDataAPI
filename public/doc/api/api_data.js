define({ "api": [
  {
    "type": "get",
    "url": "/course/class/:class_id",
    "title": "按班级获取课程数据API",
    "name": "GetCourseByClassId",
    "description": "<p>按班级获取课程数据API</p> ",
    "group": "Course",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "class_id",
            "description": "<p>课程代码</p> "
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://10.34.2.48/api/v1/course/class/:class_id"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "XN",
            "description": "<p>学年</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "XQ_ID",
            "description": "<p>学期</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "user_kcdm",
            "description": "<p>课程代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "kcmc",
            "description": "<p>课程名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "zjjs_gh",
            "description": "<p>主讲教师工号</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "zjjs_xm",
            "description": "<p>主讲教师姓名</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "user_bjdm",
            "description": "<p>班级代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "class_name",
            "description": "<p>班级名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "stimezc",
            "description": "<p>上课周次</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "JCInfo",
            "description": "<p>节次</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "room_dm",
            "description": "<p>教室代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "room",
            "description": "<p>教室名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "SKAP",
            "description": "<p>上课安排</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "KHFS",
            "description": "<p>考核方式</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "xf",
            "description": "<p>学分</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "jsxs",
            "description": "<p>讲授学时</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "NJ",
            "description": "<p>年级</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "YXMC",
            "description": "<p>院系名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "ZYDM_USER",
            "description": "<p>专业代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "ZYMC",
            "description": "<p>专业名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "kclb1mc",
            "description": "<p>课程类别</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "zongxs",
            "description": "<p>总学时</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "sjxs",
            "description": "<p>实践学时</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "stu_sum",
            "description": "<p>上课总人数</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "调用示例:",
        "content": "curl -i http://10.34.2.48/api/v1/course/class/2014040202",
        "type": "json"
      },
      {
        "title": "返回JSON示例:",
        "content": "\n{\n      \"XN\": \"2011\",\n      \"XQ_ID\": \"0\",\n      \"user_kcdm\": \"031088    \",\n      \"kcmc\": \"计算机文化\",\n      \"zjjs_gh\": \"0002234\",\n      \"zjjs_xm\": \"范兴亮\",\n      \"user_bjdm\": \"2011223201\",\n      \"class_name\": \"11软件技术2班\",\n      \"stimezc\": \"10-16,18\",\n      \"JCInfo\": \"7-8\",\n      \"room_dm\": \"2033307             \",\n      \"room\": \"修德楼C307\",\n      \"SKAP\": \"[10-16,18周]二[7-8节]\",\n      \"KHFS\": \"期考      \",\n      \"xf\": \"4.0\",\n      \"jsxs\": \"24.0\",\n      \"NJ\": \"2011\",\n      \"YXMC\": \"电子信息工程学院\",\n      \"ZYDM_USER\": \"2232      \",\n      \"ZYMC\": \"软件技术(应用软件开发)\",\n      \"kclb1mc\": \"必修课\",\n      \"zongxs\": \"64.0\",\n      \"sjxs\": \"40\",\n      \"stu_sum\": \"75\"\n}",
        "type": "json"
      }
    ],
    "filename": "app/controllers/ApiController.php",
    "groupTitle": "Course"
  },
  {
    "type": "get",
    "url": "/course/specialty/:spec_id",
    "title": "按专业获取课程数据API",
    "name": "GetCourseBySpecId",
    "description": "<p>按专业获取课程数据API</p> ",
    "group": "Course",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "spec_id",
            "description": "<p>课程代码</p> "
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://10.34.2.48/api/v1/course/specialty/:spec_id"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "XN",
            "description": "<p>学年</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "XQ_ID",
            "description": "<p>学期</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "user_kcdm",
            "description": "<p>课程代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "kcmc",
            "description": "<p>课程名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "zjjs_gh",
            "description": "<p>主讲教师工号</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "zjjs_xm",
            "description": "<p>主讲教师姓名</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "user_bjdm",
            "description": "<p>班级代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "class_name",
            "description": "<p>班级名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "stimezc",
            "description": "<p>上课周次</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "JCInfo",
            "description": "<p>节次</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "room_dm",
            "description": "<p>教室代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "room",
            "description": "<p>教室名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "SKAP",
            "description": "<p>上课安排</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "KHFS",
            "description": "<p>考核方式</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "xf",
            "description": "<p>学分</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "jsxs",
            "description": "<p>讲授学时</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "NJ",
            "description": "<p>年级</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "YXMC",
            "description": "<p>院系名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "ZYDM_USER",
            "description": "<p>专业代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "ZYMC",
            "description": "<p>专业名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "kclb1mc",
            "description": "<p>课程类别</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "zongxs",
            "description": "<p>总学时</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "sjxs",
            "description": "<p>实践学时</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "stu_sum",
            "description": "<p>上课总人数</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "调用示例:",
        "content": "curl -i http://10.34.2.48/api/v1/course/specialty/2232",
        "type": "json"
      },
      {
        "title": "返回JSON示例:",
        "content": "\n{\n      \"XN\": \"2011\",\n      \"XQ_ID\": \"0\",\n      \"user_kcdm\": \"031088    \",\n      \"kcmc\": \"计算机文化\",\n      \"zjjs_gh\": \"0002234\",\n      \"zjjs_xm\": \"范兴亮\",\n      \"user_bjdm\": \"2011223201\",\n      \"class_name\": \"11软件技术2班\",\n      \"stimezc\": \"10-16,18\",\n      \"JCInfo\": \"7-8\",\n      \"room_dm\": \"2033307             \",\n      \"room\": \"修德楼C307\",\n      \"SKAP\": \"[10-16,18周]二[7-8节]\",\n      \"KHFS\": \"期考      \",\n      \"xf\": \"4.0\",\n      \"jsxs\": \"24.0\",\n      \"NJ\": \"2011\",\n      \"YXMC\": \"电子信息工程学院\",\n      \"ZYDM_USER\": \"2232      \",\n      \"ZYMC\": \"软件技术(应用软件开发)\",\n      \"kclb1mc\": \"必修课\",\n      \"zongxs\": \"64.0\",\n      \"sjxs\": \"40\",\n      \"stu_sum\": \"75\"\n}",
        "type": "json"
      }
    ],
    "filename": "app/controllers/ApiController.php",
    "groupTitle": "Course"
  },
  {
    "type": "get",
    "url": "/school/classes",
    "title": "全部班级数据API",
    "name": "GetClasses",
    "description": "<p>获取班级数据API</p> ",
    "group": "School",
    "version": "0.1.0",
    "sampleRequest": [
      {
        "url": "http://10.34.2.48/api/v1/school/classes"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "Dept_id",
            "description": "<p>部门ID</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "Spec_id",
            "description": "<p>专业ID</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "class_id",
            "description": "<p>班级ID</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "class_name",
            "description": "<p>班级名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "myear",
            "description": "<p>年级</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "years",
            "description": "<p>学制</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "调用示例:",
        "content": "curl -i http://10.34.2.48/api/v1/school/classes",
        "type": "json"
      },
      {
        "title": "返回JSON示例:",
        "content": "\n{\n      \"Dept_id\": \"03\",\n      \"Spec_id\": \"0307      \",\n      \"class_id\": \"2004030701\",\n      \"class_name\": \"04机电\",\n      \"myear\": \"2004\",\n      \"years\": \"3\"\n}",
        "type": "json"
      }
    ],
    "filename": "app/controllers/ApiController.php",
    "groupTitle": "School"
  },
  {
    "type": "get",
    "url": "/school/departments",
    "title": "全部学校部门数据API",
    "name": "Getdepartments",
    "description": "<p>获取学校部门数据API</p> ",
    "group": "School",
    "version": "0.1.0",
    "sampleRequest": [
      {
        "url": "http://10.34.2.48/api/v1/school/departments"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "Dept_id",
            "description": "<p>部门ID</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "Dept_name",
            "description": "<p>部门名称</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "调用示例:",
        "content": "curl -i http://10.34.2.48/api/v1/school/departments",
        "type": "json"
      },
      {
        "title": "返回JSON示例:",
        "content": "\n{\n      \"Dept_id\": \"03\",\n      \"Dept_name\": \"机电工程学院\"\n}",
        "type": "json"
      }
    ],
    "filename": "app/controllers/ApiController.php",
    "groupTitle": "School"
  },
  {
    "type": "get",
    "url": "/school/specialty",
    "title": "全部专业数据API",
    "name": "Getspecialty",
    "description": "<p>获取专业数据API</p> ",
    "group": "School",
    "version": "0.1.0",
    "sampleRequest": [
      {
        "url": "http://10.34.2.48/api/v1/school/specialty"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "Dept_id",
            "description": "<p>部门ID</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "Spec_id",
            "description": "<p>专业ID</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "Spec_name",
            "description": "<p>专业名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "Guo_id",
            "description": "<p>高职专业代码</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "调用示例:",
        "content": "curl -i http://10.34.2.48/api/v1/school/specialty",
        "type": "json"
      },
      {
        "title": "返回JSON示例:",
        "content": "\n{\n      \"Dept_id\": \"04\",\n      \"Spec_id\": \"0412      \",\n      \"Spec_name\": \"金融学(本科)\",\n      \"Guo_id\": \"020104\"\n}",
        "type": "json"
      }
    ],
    "filename": "app/controllers/ApiController.php",
    "groupTitle": "School"
  },
  {
    "type": "get",
    "url": "/score/course/:course_id",
    "title": "按课程ID获取成绩数据API",
    "name": "GetScoreByCourseID",
    "description": "<p>按课程ID获取成绩数据API</p> ",
    "group": "Score",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "course_id",
            "description": "<p>课程代码</p> "
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://10.34.2.48/api/v1/score/course/:course_id"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "student_id",
            "description": "<p>学号</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "student_name",
            "description": "<p>学生姓名</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "dept_name",
            "description": "<p>院系</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "class_name",
            "description": "<p>班级名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "spec_name",
            "description": "<p>专业名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "KCDM",
            "description": "<p>课程代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "course_name",
            "description": "<p>课程名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "PSCJ",
            "description": "<p>平时成绩</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "QMCJ",
            "description": "<p>期末成绩</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "KSCJ",
            "description": "<p>总成绩</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "调用示例:",
        "content": "curl -i http://10.34.2.48/api/v1/score/course/030456",
        "type": "json"
      },
      {
        "title": "返回JSON示例:",
        "content": "\n{\n    \"student_id\": \"0404360     \",\n    \"student_name\": \"朱恒\",\n    \"dept_name\": \"电子信息工程学院\",\n    \"class_name\": \"04通信\",\n    \"spec_name\": \"通信工程\",\n    \"KCDM\": \"030456\",\n    \"course_name\": \"模拟电子电路\",\n    \"PSCJ\": \"88.00\",\n    \"QMCJ\": \"85.00\",\n    \"KSCJ\": \"86.00\"\n}",
        "type": "json"
      }
    ],
    "filename": "app/controllers/ApiController.php",
    "groupTitle": "Score"
  },
  {
    "type": "get",
    "url": "/score/student/:student_id",
    "title": "按学号获取成绩数据API",
    "name": "GetScoreByStudentID",
    "description": "<p>按学号获取成绩数据API</p> ",
    "group": "Score",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "student_id",
            "description": "<p>学号</p> "
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://10.34.2.48/api/v1/score/student/:student_id"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "student_id",
            "description": "<p>学号</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "student_name",
            "description": "<p>学生姓名</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "dept_name",
            "description": "<p>院系</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "class_name",
            "description": "<p>班级名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "spec_name",
            "description": "<p>专业名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "KCDM",
            "description": "<p>课程代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "course_name",
            "description": "<p>课程名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "PSCJ",
            "description": "<p>平时成绩</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "QMCJ",
            "description": "<p>期末成绩</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "KSCJ",
            "description": "<p>总成绩</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "调用示例:",
        "content": "curl -i http://10.34.2.48/api/v1/score/student/0404369",
        "type": "json"
      },
      {
        "title": "返回JSON示例:",
        "content": "\n{\n    \"student_id\": \"0404360\",\n    \"student_name\": \"朱恒\",\n    \"dept_name\": \"电子信息工程学院\",\n    \"class_name\": \"04通信\",\n    \"spec_name\": \"通信工程\",\n    \"KCDM\": \"030456\",\n    \"course_name\": \"模拟电子电路\",\n    \"PSCJ\": \"88.00\",\n    \"QMCJ\": \"85.00\",\n    \"KSCJ\": \"86.00\"\n}",
        "type": "json"
      }
    ],
    "filename": "app/controllers/ApiController.php",
    "groupTitle": "Score"
  },
  {
    "type": "get",
    "url": "/student/id/:student_id",
    "title": "按学号获取学生数据API",
    "name": "GetStudentById",
    "description": "<p>按学号获取学生数据API</p> ",
    "group": "Student",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "student_id",
            "description": "<p>班级ID</p> "
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://10.34.2.48/api/v1/student/id/:student_id"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "user_xh",
            "description": "<p>学号</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "student_name",
            "description": "<p>学生姓名</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "sex",
            "description": "<p>性别</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "NJ",
            "description": "<p>年级</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "DM",
            "description": "<p>院系代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "user_dm",
            "description": "<p>专业代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "spec_name",
            "description": "<p>专业名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "GBZYDM",
            "description": "<p>国标专业代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "XZ",
            "description": "<p>学制</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "BJDM",
            "description": "<p>班级代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "class_name",
            "description": "<p>班级名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "xjzt",
            "description": "<p>学籍状态</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "sfzx",
            "description": "<p>是否在校</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "GKKSH",
            "description": "<p>高考考生号</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "GKZKZH",
            "description": "<p>高考准考证号</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "CSRQ",
            "description": "<p>出生日期</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "nation",
            "description": "<p>民族</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "politic",
            "description": "<p>政治面貌</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "RXCHJ",
            "description": "<p>入学成绩</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "RXSJ",
            "description": "<p>入学时间</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "dqmc",
            "description": "<p>地区名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "DQDM",
            "description": "<p>地区代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "SJR",
            "description": "<p>联系人</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "LXDH",
            "description": "<p>联系电话</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "YZBM",
            "description": "<p>邮政编码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "email",
            "description": "<p>电子邮箱</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "in_specialty",
            "description": "<p>录取专业</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "调用示例:",
        "content": "curl -i http://10.34.2.48/api/v1//student/id/1400326",
        "type": "json"
      },
      {
        "title": "返回JSON示例:",
        "content": "\n{\n      \"user_xh\": \"1300577\",\n      \"student_name\": \"岑袁\",\n      \"sex\": \"男\",\n      \"NJ\": \"2013\",\n      \"DM\": \"04\",\n      \"user_dm\": \"0404      \",\n      \"spec_name\": \"市场营销\",\n      \"GBZYDM\": \"110202\",\n      \"XZ\": \"3\",\n      \"BJDM\": \"2013040404\",\n      \"class_name\": \"13市场营销4班\",\n      \"xjzt\": \"1 \",\n      \"sfzx\": \"0\",\n      \"GKKSH\": \"13500130150195\",\n      \"GKZKZH\": \"\",\n      \"CSRQ\": \"Jul 12 1994 12:00:00:000AM\",\n      \"nation\": \"汉族\",\n      \"politic\": \"共青团员                      \",\n      \"RXCHJ\": \"287\",\n      \"RXSJ\": \"Aug 18 2013 09:48:55:717AM\",\n      \"dqmc\": null,\n      \"DQDM\": null,\n      \"SJR\": \"\",\n      \"LXDH\": \"13628204949\",\n      \"YZBM\": \"404343\",\n      \"email\": null,\n      \"in_specialty\": \"市场营销\"\n  }",
        "type": "json"
      }
    ],
    "filename": "app/controllers/ApiController.php",
    "groupTitle": "Student"
  },
  {
    "type": "get",
    "url": "/student/class/:class_id",
    "title": "按班级ID获取学生数据API",
    "name": "GetStudentsByClassId",
    "description": "<p>按班级ID获取学生数据API</p> ",
    "group": "Student",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "class_id",
            "description": "<p>班级ID</p> "
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://10.34.2.48/api/v1/student/class/:class_id"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "user_xh",
            "description": "<p>学号</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "student_name",
            "description": "<p>学生姓名</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "sex",
            "description": "<p>性别</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "NJ",
            "description": "<p>年级</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "DM",
            "description": "<p>院系代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "user_dm",
            "description": "<p>专业代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "spec_name",
            "description": "<p>专业名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "GBZYDM",
            "description": "<p>国标专业代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "XZ",
            "description": "<p>学制</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "BJDM",
            "description": "<p>班级代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "class_name",
            "description": "<p>班级名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "xjzt",
            "description": "<p>学籍状态</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "sfzx",
            "description": "<p>是否在校</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "GKKSH",
            "description": "<p>高考考生号</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "GKZKZH",
            "description": "<p>高考准考证号</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "CSRQ",
            "description": "<p>出生日期</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "nation",
            "description": "<p>民族</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "politic",
            "description": "<p>政治面貌</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "RXCHJ",
            "description": "<p>入学成绩</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "RXSJ",
            "description": "<p>入学时间</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "dqmc",
            "description": "<p>地区名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "DQDM",
            "description": "<p>地区代码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "SJR",
            "description": "<p>联系人</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "LXDH",
            "description": "<p>联系电话</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "YZBM",
            "description": "<p>邮政编码</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "email",
            "description": "<p>电子邮箱</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "in_specialty",
            "description": "<p>录取专业</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "调用示例:",
        "content": "curl -i http://10.34.2.48/api/v1//student/class/2014040202",
        "type": "json"
      },
      {
        "title": "返回JSON示例:",
        "content": "\n{\n      \"user_xh\": \"1300577\",\n      \"student_name\": \"岑袁\",\n      \"sex\": \"男\",\n      \"NJ\": \"2013\",\n      \"DM\": \"04\",\n      \"user_dm\": \"0404      \",\n      \"spec_name\": \"市场营销\",\n      \"GBZYDM\": \"110202\",\n      \"XZ\": \"3\",\n      \"BJDM\": \"2013040404\",\n      \"class_name\": \"13市场营销4班\",\n      \"xjzt\": \"1 \",\n      \"sfzx\": \"0\",\n      \"GKKSH\": \"13500130150195\",\n      \"GKZKZH\": \"\",\n      \"CSRQ\": \"Jul 12 1994 12:00:00:000AM\",\n      \"nation\": \"汉族\",\n      \"politic\": \"共青团员                      \",\n      \"RXCHJ\": \"287\",\n      \"RXSJ\": \"Aug 18 2013 09:48:55:717AM\",\n      \"dqmc\": null,\n      \"DQDM\": null,\n      \"SJR\": \"\",\n      \"LXDH\": \"13628204949\",\n      \"YZBM\": \"404343\",\n      \"email\": null,\n      \"in_specialty\": \"市场营销\"\n  }",
        "type": "json"
      }
    ],
    "filename": "app/controllers/ApiController.php",
    "groupTitle": "Student"
  },
  {
    "type": "get",
    "url": "/teacher/department/:dept_id",
    "title": "按部门ID获取教师数据API",
    "name": "GetTeacherByDeptID",
    "description": "<p>按部门ID获取教师数据API，注意此API受青果系统中数据不全影响</p> ",
    "group": "Teacher",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "dept_id",
            "description": "<p>部门ID</p> "
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://10.34.2.48/api/v1/teacher/department/:dept_id"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "teacher_id",
            "description": "<p>教师青果工号</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "teacher_name",
            "description": "<p>教师姓名</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "birthday",
            "description": "<p>教师出生日期</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "email",
            "description": "<p>邮箱</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "start_year",
            "description": "<p>入校年份</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "graduate_subject",
            "description": "<p>毕业专业</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "sex",
            "description": "<p>性别</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "dept_id",
            "description": "<p>部门ID</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "dept_name",
            "description": "<p>部门名称</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "调用示例:",
        "content": "curl -i http://10.34.2.48/api/v1/teacher/department/22",
        "type": "json"
      },
      {
        "title": "返回JSON示例:",
        "content": "\n{\n      \"teacher_id\": \"0000010\",\n      \"teacher_name\": \"单××\",\n      \"birthday\": \"19780701\",\n      \"email\": \"shankefeng@cqdd.cq.cn\",\n      \"start_year\": \"2000\",\n      \"graduate_subject\": \"毕业于重庆理工大学会计学专业\",\n      \"sex\": \"男\",\n      \"dept_id\": \"22\",\n      \"dept_name\": \"电子信息工程学院\"\n}",
        "type": "json"
      }
    ],
    "filename": "app/controllers/ApiController.php",
    "groupTitle": "Teacher"
  },
  {
    "type": "get",
    "url": "/teacher/id/:teacher_id",
    "title": "按ID获取教师数据API",
    "name": "GetTeacherByID",
    "description": "<p>获取单个教师数据API，注意此API受青果系统中数据不全影响</p> ",
    "group": "Teacher",
    "version": "0.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "teacher_id",
            "description": "<p>青果教务系统工号</p> "
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://10.34.2.48/api/v1/teacher/id/:teacher_id"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "teacher_id",
            "description": "<p>教师青果工号</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "teacher_name",
            "description": "<p>教师姓名</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "birthday",
            "description": "<p>教师出生日期</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "email",
            "description": "<p>邮箱</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "start_year",
            "description": "<p>入校年份</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "graduate_subject",
            "description": "<p>毕业专业</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "sex",
            "description": "<p>性别</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "dept_id",
            "description": "<p>部门ID</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "dept_name",
            "description": "<p>部门名称</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>Array</p> ",
            "optional": false,
            "field": "projects",
            "description": "<p>质量工程项目</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "调用示例:",
        "content": "curl -i http://10.34.2.48/api/v1/teacher/id/0001692",
        "type": "json"
      },
      {
        "title": "返回JSON示例:",
        "content": "\n{\n      \"teacher_id\": \"0000010\",\n      \"teacher_name\": \"单××\",\n      \"birthday\": \"19780701\",\n      \"email\": \"shankefeng@cqdd.cq.cn\",\n      \"start_year\": \"2000\",\n      \"graduate_subject\": \"毕业于重庆理工大学会计学专业\",\n      \"sex\": \"男\",\n      \"dept_id\": \"22\",\n      \"dept_name\": \"电子信息工程学院\"\n}",
        "type": "json"
      }
    ],
    "filename": "app/controllers/ApiController.php",
    "groupTitle": "Teacher"
  },
  {
    "type": "get",
    "url": "/teacher/all",
    "title": "全部教师数据API",
    "name": "GetTeachers",
    "description": "<p>获取教师数据API，注意此API受青果系统中数据不全影响</p> ",
    "group": "Teacher",
    "version": "0.1.0",
    "sampleRequest": [
      {
        "url": "http://10.34.2.48/api/v1/teacher/all"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "teacher_id",
            "description": "<p>教师青果工号</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "teacher_name",
            "description": "<p>教师姓名</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "birthday",
            "description": "<p>教师出生日期</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "email",
            "description": "<p>邮箱</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "start_year",
            "description": "<p>入校年份</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "graduate_subject",
            "description": "<p>毕业专业</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "sex",
            "description": "<p>性别</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "dept_id",
            "description": "<p>部门ID</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "dept_name",
            "description": "<p>部门名称</p> "
          }
        ]
      }
    },
    "examples": [
      {
        "title": "调用示例:",
        "content": "curl -i http://10.34.2.48/api/v1/teacher/all",
        "type": "json"
      },
      {
        "title": "返回JSON示例:",
        "content": "\n{\n      \"teacher_id\": \"0000010\",\n      \"teacher_name\": \"单××\",\n      \"birthday\": \"19780701\",\n      \"email\": \"shankefeng@cqdd.cq.cn\",\n      \"start_year\": \"2000\",\n      \"graduate_subject\": \"毕业于重庆理工大学会计学专业\",\n      \"sex\": \"男\",\n      \"dept_id\": \"22\",\n      \"dept_name\": \"电子信息工程学院\"\n}",
        "type": "json"
      }
    ],
    "filename": "app/controllers/ApiController.php",
    "groupTitle": "Teacher"
  }
] });