define({
  "name": "高职教育教学数据Open API模型文档",
  "version": "0.1.0",
  "description": "&copy;重庆工商职业学院2014年科研青年项目《高职教育教学数据Open API构建研究》",
  "title": "高职教育教学数据Open API",
  "url": "http://10.34.2.48/api/v1",
  "sampleUrl": "http://10.34.2.48/api/v1",
  "header": {
    "title": "文档说明",
    "content": "<h2>文档说明</h2>\n<p>本研究构建的高职教育教学数据Open API仅为功能示例。（强烈建议使用非IE内核浏览器）</p>\n<p>API中有部分是公开数据，不需要授权与验证即可获取，部分则需要通过Oauth2授权。需要授权的API，提交请求时需要提供未过期的Access Token。</p>\n"
  },
  "footer": {
    "title": "后续研究",
    "content": "<h1>后续研究</h1>\n<p>本项目研究了高职教育教学Open API的构建方法与可行的技术实现，后续将继续丰富扩展Open API的种类与数量，为我校教育信息化的推进提供可参考的数据架构方案。</p>\n<p>由于数据的获取特别是青果教务系统中的数据使用了大量的视图，请求的响应可能会稍慢，后续研究将添加Cache以提供更快速的数据响应。此外，添加https支持也是Open API必须的一种保护数据安全措施。</p>\n"
  },
  "template": {
    "withCompare": true,
    "withGenerator": true
  },
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2015-06-09T02:53:24.198Z",
    "url": "http://apidocjs.com",
    "version": "0.13.1"
  }
});