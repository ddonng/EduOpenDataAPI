<?php
use Phalcon\Mvc\Micro\Collection as MicroCollection;
error_reporting(E_ALL);
ini_set('memory_limit', '-1');
ini_set( 'display_errors' , true );
try {
    //Shutdown model NULL Validations
    \Phalcon\Mvc\Model::setup(array(
        'notNullValidations' => false
    ));
    /**
     * Read the configuration
     */
    $config = include __DIR__ . "/../app/config/config.php";

    /**
     * Read auto-loader
     */
    include __DIR__ . "/../app/config/loader.php";

    /**
     * Read services
     */
    include __DIR__ . "/../app/config/services.php";

    /**
     * Handle the request
     */
    // $application = new \Phalcon\Mvc\Application($di);
    $app  = new  \Phalcon\Mvc\Micro($di);
    // echo $application->handle()->getContent();
    if(!$app->apc->get("scope_meaning")){
        $app->apc->save('scope_meaning',array(
                "basic" => "个人基础数据",
                "pinfo" => "质量工程数据"
            ));
    }

    $posts = new MicroCollection();

    //Set the main handler. ie. a controller instance
    $posts->setHandler('IndexController', true);

    //Set a common prefix for all routes
    $posts->setPrefix('/index');

    //Use the method 'index' in PostsController
    $posts->get('/', 'indexAction');
    $posts->get('/index', 'indexAction');

    //Use the method 'show' in PostsController
    $posts->get('/show/{slug}', 'show');
    $app->mount($posts);



    $oauth = new MicroCollection();
    $oauth->setHandler('OauthController',true);
    $oauth->setPrefix('/oauth2');
    $oauth->get('/','indexAction');
    $oauth->get('/token','tokenAction');
    $oauth->post('/token','tokenAction');
    $oauth->get('/authorize','authorizeAction');
    $oauth->post('/authorize','authorizeAction');
    $app->mount($oauth);

 
    $account = new MicroCollection();
    $account->setHandler('AccountController',true);
    $account->setPrefix('/account');
    $account->get('/','indexAction');
    $account->get('/register','registerAction');
    $account->get('/captcha','captchaAction');
    $account->post('/addUser','addUserAction');
    $account->get('/captchaImg','captchaImgAction');
    $account->post('/repwd','resetPasswordAction');
    $app->mount($account);

    /*
    * API 
    */
    $api = new MicroCollection();
    $api->setHandler('ApiController',true);
    $api->setPrefix('/api/v1');
    $api->get('/','indexAction');
    $api->get('/teacher/info/{teacher_id}',"getTeacherByIdAction");
    $api->get('/teacher/feature/{teacher_id}',"getTeacherFeatureAction");
    // $api->post('/teacher/info/{teacher_id}',"getTeacherByIdAction");
    $api->get('/custom/project/{type}','customProjectAction');
    $api->get('/custom/projects','getCustomProjectsAction');
    $api->get('/custom/features','getFeaturesAction');
    $api->get('/custom/project/fields/{type}','getProjectFieldsAction');

    $api->get('/projects/all','getAllProjectsDetailAction');
    $api->get('/projects/{teacher_id}','getTeacherProjectsAction');
    $api->post('/projects/add','addProjectAction');
    $api->post('/projects/find','getProjectsAction');

    $api->post('/project/find','getProjectAction');



    $api->get('/school/departments','getDepartmentsAction');
    $api->get('/school/classes','getClassesAction');
    $api->get('/school/specialty','getSpecialtyAction');

    $api->get('/teacher/all','getTeachersAction');
    $api->get('/teacher/id/{teacher_id}','getTeacherByKSIdAction');
    $api->get('/teacher/department/{dept_id}','getTeachersByDeptIdAction');


    $api->get('/student/class/{class_id}','getStudentsByClassIdAction');
    $api->get('/student/id/{student_id}','getStudentByIdAction');


    $api->get('/score/course/{course_id}','getScoreByCourseIdAction');
    $api->get('/score/student/{student_id}','getScoreByStudentIdASction');


    $api->get('/course/specialty/{spec_id}','getCourseBySpecIdAction');
    $api->get('/course/class/{class_id}','getCourseByClassIdAction');
    $app->mount($api);




    $app->handle();
} catch (\Exception $e) {
    echo $e->getMessage();
}
