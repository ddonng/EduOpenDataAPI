<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Cache\Backend\Apc as ApcCache;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
}, true);

/**
 * Setting up the view component
 */
$di->set('view', function () use ($config) {

    $view = new View();

    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines(array(
        '.volt' => function ($view, $di) use ($config) {

            $volt = new VoltEngine($view, $di);

            $volt->setOptions(array(
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_'
            ));

            return $volt;
        },
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ));

    return $view;
}, true);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config) {
    return new DbAdapter(array(
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname
    ));
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
* MongoDB DI
*/

$di->set('mongo',function(){
    //第一次连接会报错remote server cannot connected
    
    try {
        $myMongoClient = new MongoClient("mongodb://mongoLink:27017",array("username" => "ddonng", "password" => "6623266"));
        $mongo = $myMongoClient->selectDB('project');
    } catch( expetion $e) {
        $MaxRetries = 5;
        for( $Counts = 1; $Counts <= $MaxRetries; $Counts ++ ) {
            try {
                $myMongoClient = new MongoClient("mongodb://mongoLink:27017",array("username" => "ddonng", "password" => "6623266")); 
                $mongo = $myMongoClient->selectDB('project');
            } catch( expetion $e){
                continue;
            }
            exit;
        }
    }

    return $mongo;
});

$di->set('collectionManager', function(){
      return new Phalcon\Mvc\Collection\Manager();
 });
/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

$di->set('crypt', function() {
    $crypt = new Phalcon\Crypt();
    $crypt->setKey('#@weij8$=dp?.aweiddong//j1V$'); 
    return $crypt;
});

$di->set('cookies', function() {
    $cookies = new Phalcon\Http\Response\Cookies();
    $cookies->useEncryption(true);
    return $cookies;
});

/*
* 设置一个apcu存储scope的含义，后续可能加其他
*/
$di->set('apc',function(){
        // Cache the files for 2 days using a Data frontend
    $frontCache = new Phalcon\Cache\Frontend\Data(array(
        "lifetime" => 172800
    ));
    $apc = new ApcCache($frontCache,array(
      'prefix' => 'app-data'
    ));

    return $apc;
});

$di->set('memcached',function(){
    // Cache data for 2 days
    $frontCache = new Phalcon\Cache\Frontend\Data(array(
        "lifetime" => 172800
    ));

    //Create the Cache setting memcached connection options
    $memcached = new Phalcon\Cache\Backend\Libmemcached($frontCache, array(
        'servers' => array(
           array('host' => 'localhost',
             'port' => 11211,
             'weight' => 1),
           ),
        'client' => array(
           Memcached::OPT_HASH => Memcached::HASH_MD5,
           Memcached::OPT_PREFIX_KEY => 'gpm.',
           )
    ));
    return $memcached;
});


$di->set('notify', function() use($di){
    $notify = new Notification($di);
    return $notify; 
});

$di->set('captcha', function(){
    $captcha = new Captcha();
    return $captcha;
});

$di->set('queue', function(){
    //default 127.0.0.1:11300 
    $queue = new Phalcon\Queue\Beanstalk();
    return $queue;
});