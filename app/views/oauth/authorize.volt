{% include "partials/header.volt" %}
<body>
<div class="login-layout">
<div class="main-container">
<div class="main-content">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<div class="logincontainer">
				<div class="center">
				<h2>
					<i class="icon-leaf green"></i>
					<span class="white">质量工程开放数据平台</span>
				</h2>
					<h6 class="blue">&copy; 渝电大（工商职院）教务项目管理科</h6>
				</div>
				<div class="space-6"></div>

				<div class="position-relative">

				<form method="post" class="msform">
					<div id="login-box" class="login-box visible widget-box no-border">
						<div class="widget-body">
							<div class="widget-main">
							<div class="header">
								<h6 class="blue lighter bigger">授权“<a href="javascript:void(0)" class="red">{{client_name}}</a>”访问您如下数据</h6>
								<p>
								{% for scope in scope_arr %}
									<input type="checkbox" value="{{scope}}" checked="true" disabled="true" >{{scope_meaning[scope]}}
								{% endfor %}
								</p>
								</div>
								<div class="login-field space-6"></div>

									<fieldset class="login-container">
									<div ng-app="myapp">
									<div ng-controller = "loginController">

										<label class="block clearfix">
											<span class="block input-icon input-icon-right">
												<input name="userid" id="userid" ng-model="user.userid" type="text" class="form-control" placeholder="请输入您的注册Email" autocomplete="off" ng-class="{'border-danger':stat.lstat=='invalid'}" ng-blur="checkUserID()" tabindex="1" {% if result=="NO_USER" %}autofocus{% endif %} {% if user_id %} ng-init="user.userid='{{user_id}}'" {% endif%}/>
												<i class="icon-user"></i>
											</span>
											<span class="danger" ng-if="stat.lstat=='invalid'">*[[stat.lmsg]]</span>
										{% if result=="NO_USER" %}
										<span class="danger" ng-if="stat.lstat!='invalid' &&  stat.lstat!='valid'" >*用户名{{user_id}}不存在，请重新输入</span>
										{% endif %}
										</label>

										<label class="block clearfix">
											<span class="block input-icon input-icon-right">
												<input name="pwd" id="pwd" ng-model="user.pwd" type="password" class="form-control" placeholder="请输入密码"  ng-class="{'border-danger':stat.pstat=='empty'}" ng-blur="blurPwd()" ng-change="changePwd()" tabindex="2" {% if result=="WRONG_PWD" %}autofocus{% endif %}/>
												<i class="icon-lock"></i>
											</span>
											<span class="danger" ng-if="stat.pstat=='empty'">*密码有误，不能为空或小于5位</span>
											{% if result=="WRONG_PWD" %}
											<span class="danger" ng-if="stat.pstat!='empty' && stat.pstat!='full'">*密码不匹配，请重新输入</span>
											{% endif %}

										</label>

										</div></div>


										<div class="space"></div>
										<div class="clearfix">

											<input type="submit" id="loginbt" class="btn btn-lg btn-primary btn-block" value="登录" tabindex="3" />
										</div>

									</fieldset>

								</form>

								<div class="social-or-login center">
									<span class="bigger-110">使用Chrome或FireFox浏览器</span>
								</div>

								<div class="social-login center">
									<a class="btn btn-primary">
										{{image("public/img/chrome.png")}}
									</a>

									<a class="btn btn-info">
										{{image("public/img/firefox.png")}}
									</a>

									<a class="btn btn-success">
										{{image("public/img/safari.png")}}
									</a>
								</div>
							</div><!-- /widget-main -->

							<div class="toolbar clearfix">
								<div>
									<a href="#" onclick="show_box('forgot-box'); return false;" class="forgot-password-link" tabindex="4">
										<i class="glyphicon glyphicon-arrow-left"></i>
										找回密码
									</a>
								</div>

								<div>
									<a href="{{ url('account/register') }}"  class="user-signup-link" tabindex="5">
										新用户注册
										<i class="glyphicon glyphicon-arrow-right"></i>
									</a>
								</div>
							</div>
						</div><!-- /widget-body -->
					</div><!-- /login-box -->

					<div id="forgot-box" class="forgot-box widget-box no-border" ng-app="forgotApp">
						<div class="widget-body" ng-controller="forgotController">
							<div class="widget-main">
								<h4 class="header red lighter bigger">
									<i class="icon-key"></i>
									找回密码
								</h4>

								<div class="space-6"></div>
								<p>
									请输入您的注册手机号码与姓名
								</p>

								
									<fieldset>
          <label class="block clearfix">
           <span class="block input-icon input-icon-right">
            <input type="mobile_phone" class="form-control" placeholder="输入手机号码" ng-model="teacher.phoneNumber" autocomplete="off" ng-class="{'border-danger':stat.phoneStat=='invalid'}" ng-change="checkPhone()"/>
            <i class="icon-envelope"></i>
           </span>
          </label>
          <label class="block clearfix">
           <span class="block input-icon input-icon-right">
            <input type="tname" class="form-control tname-input" placeholder="输入您的姓名" ng-model="teacher.tname"/>
            <i class="icon-envelope"></i>
           </span>
          </label>
										<label class="block clearfix">
											<span class="block input-icon input-icon-right">
												<input type="tname" class="form-control captcha-input" placeholder="输入验证码" ng-model="teacher.captcha"/>
            <img src="/account/captchaImg" onClick="javascript:this.src='/account/captchaImg&tm='+Math.random();" />
												<i class="icon-envelope"></i>
											</span>
										</label>

										<div class="clearfix">
          <em class="label" ng-class="{'label-danger':stat.code==='0','label-success':stat.code==='1'}">[[stat.msg]]</em>
											<button type="button" class="width-35 pull-right btn btn-sm btn-danger" ng-click="resetPwd()" ng-disabled="stat.phoneStat!='valid' || teacher.tname=='' || teacher.captcha=='' || stat.code==='1'">
												<i class="icon-lightbulb"></i>
												点击发送！
											</button>
										</div>
									</fieldset>
								
							</div><!-- /widget-main -->

							<div class="toolbar center">
								<a href="#" onclick="show_box('login-box'); return false;" class="back-to-login-link">
									返回登录
									<i class="icon-arrow-right"></i>
								</a>
							</div>
						</div><!-- /widget-body -->
					</div><!-- /forgot-box -->

				</div><!-- /position-relative -->
			</div>
		</div><!-- /.col -->
	</div><!-- /.row -->
</div>
</div><!-- /.main-container -->
</div><!--login-layout-->

{{assets.outputJs()}}


</body>
</html>