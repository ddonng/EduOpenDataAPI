<!DOCTYPE html>
<html>
	<head>
		<title>重庆广播电视大学</title>
		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		{{assets.outputCss()}}
	</head>
	<body>
		{{ content() }}
		{{assets.outputJs()}}
	</body>
</html>