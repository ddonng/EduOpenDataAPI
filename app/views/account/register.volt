{% include "partials/header.volt" %}
<body>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
	    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mymenu">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">质量工程开放数据平台</a>
    </div>
	<div class="collapse navbar-collapse pull-right" id="mymenu">
	 <ul class="nav navbar-nav">
	 <li><a href="http://app.cqtbi.edu.cn/login">若要访问“质量工程项目申报与经费报销系统”,点击此处</a></li>
	 </ul>
	</div>
  </div>
</nav>

<div ng-app="myApp">
<div ng-controller = "registerController">
<!-- multistep form -->
<form id="msform" name="msform">
	<!-- progressbar -->
	<ul id="progressbar">
		<li class="active">创建账户</li>
		<li>个人信息</li>
		<li>部门</li>
		<li>提交</li>
	</ul>
	<!-- fieldsets -->
	<fieldset>
		<h2 class="fs-title">创建您的账户</h2>
		<h3 class="fs-subtitle">使用真实的手机号码注册</h3>
		<input type="text" name="mobile_phone" ng-class="{'border-danger':stat=='unchecked'}" ng-disabled="stat=='success'" placeholder="您的手机号码" data-template="/public/angular/tpl/popover.tpl.html" data-title="[[popover.title]]" data-content="[[popover.content]]" data-trigger="manual" data-animation="am-flip-x" ng-change="checkPhone()" ng-model="user.phoneNumber" bs-popover required tabindex="1" />
		<input type="text" name="captcha" placeholder="请输入验证码" ng-model="user.captcha" required tabindex="2" />

		<div class="inner-addon right-addon">
			<i class="glyphicon glyphicon-ok green" ng-if="pwdstat=='good'"></i>
			<i class="glyphicon glyphicon-arrow-right red" ng-if="pwdstat=='short'"></i>
			<input type="password" name="passwd" placeholder="设置您的密码，至少8位" ng-change="checkPwd()" ng-model="user.passwd" required tabindex="3" />
		</div>
		<input type="button" name="next" class="next action-button" value="下一步" ng-disabled="canext=='no'" />
	</fieldset>
	<fieldset>
		<h2 class="fs-title">个人信息</h2>
		<h3 class="fs-subtitle">请填写常用真实信息</h3>
		<div class="inner-addon right-addon">
			<i class="glyphicon glyphicon-ok green" ng-if="emailstat=='valid'"></i>
			<input type="text" name="email" placeholder="输入您的常用邮箱" ng-model="user.email" ng-change="checkEmail()" ng-class="{'border-danger':emailstat=='invalid'}" required />
		</div>
		<input type="text" name="tname" placeholder="您的姓名" ng-model="user.tname" required />
		<select name="sex" ng-model="user.sex" required>
			<option value="" selected>请选择性别</option>
			<option value="0">男</option>
			<option value="1">女</option>
		</select>
		<select name="professional_title" ng-model="user.ptitle" required>
			<option value="" selected>请选择您的职称</option>
			<option value="教授">教授</option>
			<option value="高级工程师">高级工程师</option>
			<option value="副教授">副教授</option>
			<option value="讲师">讲师</option>
			<option value="工程师">工程师</option>
			<option value="助教">助教</option>
		</select>
		<input type="button" name="previous" class="previous action-button" value="上一步" />
		<input type="button" name="next" class="next action-button" value="下一步" />
	</fieldset>
	<fieldset>
		<h2 class="fs-title">部门信息</h2>
		<h3 class="fs-subtitle">目前您所在的部门</h3>
		<select ng-model="user.isInsideSchool" ng-change="checkDept()" required>
			<option value="">您的部门所在</option>
			<option value="inside">校本部</option>
			<option value="byside">电大分校或工作站</option>
			<option value="out">校外或企业</option>
		</select>
		<select name="inside_department" ng-if="user.isInsideSchool=='inside'" ng-model="user.department">
			<option value="" selected="true">请选择校本部部门具体名称</option>
			{% for dep in inside_department %}
			<option value="{{dep.department_id}}">{{dep.department_name}}</option>
			{% endfor %}
		</select>
		<select name="byside_department" ng-if="user.isInsideSchool=='byside'" ng-model="user.department">
			<option value="" selected="true">请选择电大分校或工作站名称</option>
			{% for bydep in byside_department %}
			<option value="{{bydep.department_id}}">{{bydep.department_name}}</option>
			{% endfor %}
		</select>
		<span ng-if="user.isInsideSchool=='out'"></span>
		<input type="text" name="phone" placeholder="办公室电话（可选,格式023-42861001)" ng-model="user.officePhone" ng-change="checkOfficePhone()" ng-class="{'border-danger':ophstat=='invalid'}"/>
		<textarea name="duty" placeholder="目前工作职责说明"></textarea>
		<input type="button" name="previous" class="previous action-button" value="上一步" />
		<input type="submit" name="submit" class="next action-button" value="提交注册" ng-disabled="msform.$invalid || canreg=='no'" ng-click="addUser()" />
	</fieldset>
	<fieldset>
		<div ng-if="addstat.code=='1'">
			<h2 class="fs-title">注册成功</h2>
			<h3 class="fs-subtitle">[[addstat.msg]]即刻启程!</h3>
			<a href="">
				<p><i class="glyphicon glyphicon-ok-sign green" style="font-size:40px"></i></p>
			</a>
		</div>
		<div ng-if="addstat.code==='0'">
			<h2 class="fs-title">注册失败</h2>
			<h3 class="fs-subtitle danger">[[addstat.msg]]</h3>
		</div>
		<a class="previous"  >
			<p ng-if="addstat.code==='0' && !angular.isUndefined(addstat.code)"><i class="glyphicon glyphicon-remove red" style="font-size:40px"></i></p>
		</a>
	</fieldset>
</form>
</div></div>

{{ assets.outputJs() }}


</body>
</html>