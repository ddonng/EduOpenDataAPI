<?php
use Phalcon\Mvc\User\Plugin;
/*
* 邮件通知，采用的是sendcloud.sohu.com服务，免费200封/天
* 短信通知，采用的学校的短信网关，到达率非常高
*/
class Notification extends Plugin
{
	public function __construct($dependencyInjector)
	{
		$this->_dependencyInjector = $dependencyInjector;
	}


	private function _sendSms($mobile_phone,$content)
	{
		$wsdl = "http://61.186.170.114/sms/Service.asmx?wsdl";
		$client = new SoapClient($wsdl);

		$appID="3";
		$appToken="B656AFCCEF79BA6226E3EB6824C8B297";


		$strMd5=strtoupper(md5($appID.$appToken));

		//参数赋值
		$param = array('appId'=>$appID,'strMD5'=>$strMd5,'Mobiles'=>$mobile_phone, 'SmContent'=>$content.date('Y-m-d H:i:s',time()));

		//发送短信
		$code = $client->SendMs($param);
		return (($code->SendMsResult)=="短信发送成功")?1:0;
	}

	private function _sendEmail($email,$subject,$content)
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_URL, 'https://sendcloud.sohu.com/webapi/mail.send.json');
    //不同于登录SendCloud站点的帐号，您需要登录后台创建发信子帐号，使用子帐号和密码才可以进行邮件的发送。
		curl_setopt($ch, CURLOPT_POSTFIELDS,
			array(
				'api_user' => 'postmaster@cqtbi.sendcloud.org',
				'api_key' => 'KP4Wnexu7vFnGFlq',
				'from' => 'postmaster@mail.cqtbi.esch.cn',
				'fromname' => '渝电大(工商职院)教务处项目管理科',
				'to' => $email,
				'subject' => $subject,
				'html' => $content
				));        
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$status = curl_exec($ch);

		if($status === false) //请求失败
		{
			echo 'last error : ' . curl_error($ch);
		}

		curl_close($ch);
		$result = json_decode($status);
		return ($result->message=="success")?1:0;
	}

	/**************************************************************
	***************************SMS**********************
	**************************************************************/

	public function sendCaptchaSMS($phoneNumber,$captcha)
	{
		$content = "您请求的验证码为：".$captcha."(质量工程数据开放平台)，请于6分钟内输入，切勿泄漏。";

		return $this->_sendSms($phoneNumber,$content);

	}

	public function sendResetPwdSMS($phoneNumber,$newPwd)
	{
		$content = "您的质量工程开放数据平台，密码重置为：".$newPwd."，请登陆后自行更改！若重置非本人操作，请立即致电项目管理科";
		
		return $this->_sendSms($phoneNumber,$content);
	}


	/**************************************************************
	***************************Email**********************
	**************************************************************/
	public function sendVerificationEmail($email,$teacher_name,$verification_code)
	{
		$subject = "请激活您的账号";
		$link = $this->url->get('account/verificationEmail?code='.$verification_code);
		$content = "尊敬的".$teacher_name."：<br>请点击以下链接激活您的账号<br><a href='".$link."'>".$link."</a><br>教务处项目科";

		return $this->_sendEmail($email,$subject,$content);

	}

}