<?php
use Phalcon\Events\Event,
        Phalcon\Mvc\User\Plugin,
        Phalcon\Mvc\Dispatcher,
        Phalcon\Acl;

class Security extends Plugin
{

	public function __construct($dependencyInjector)
	{
		$this->_dependencyInjector = $dependencyInjector;
	}

	public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
	{
		// $this->flash->error("Controller_Name：".$dispatcher->getControllerName());
		// $this->flash->error("<br>Controller Class：".$dispatcher->getControllerClass());
		$this->cookies->set('ddd','huang',time()+300);
		$dispatcher->redirect(
			array(
				'controller' => 'index',
				'action' => 'index'
				)
			);
		return false;
	}

}