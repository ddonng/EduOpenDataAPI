<?php

/**
* @apiDefine Oauth2 授权数据
* 此数据接口提供非公开访问数据，请求数据时需要提供经过授权验证的Access Token
*
*/


class ApiController extends \Phalcon\Mvc\Controller
{

	//返回json格式结果
	private function send_result( $data )
	{ 
		$obj = array();
		$obj['error'] = 'success';
		$obj['data'] = $data;

		header('Content-type: application/json');
		die( json_encode( $obj ) );
	}
	private function send_error( $msg )
	{	
		$obj = array();
		$obj['error'] = 'invalid token';
		$obj['err_msg'] = $msg;
		
		header('Content-type: application/json');
		die( json_encode( $obj ) );
	}


	/**
	* 访问控制（Micro没有dispatcher:http://www.phosphorum.com/discussion/2836/dispatcher，故考虑采用这种方式）
	* $need_check_user_id>0，则要验证access_token的授权user_id与当前请求API的用户一致
	*/
	private function checkATK($need_check_user_id=FALSE,$scope=NULL)
	{
		$oauthServer=new OauthServer();
		$server = $oauthServer->getServerInstance();
		if (!$server->verifyResourceRequest(OAuth2\Request::createFromGlobals(),null,$scope)) {
			$server->getResponse()->send();
			die;
		}else{
			// access_token有效
			if($need_check_user_id && $need_check_user_id>0 )
			{
				//要验证此access_token是否为此用户授权
				$token = $server->getAccessTokenData(OAuth2\Request::createFromGlobals());
				if($token['user_id'] == $need_check_user_id){
					return true;
				}else{
					$this->send_error('The access_token should used by the user who authoried it.');
				}

			}else{
				//不验证用户关联
				return true;

			}
		}

	}


	public function indexAction()
	{

	}


	public function getTeacherByIdAction($teacher_id)
	{
	    	//$access_token = $this->request->get('access_token');
		$required_scope="basic";
		$this->checkATK($teacher_id,$required_scope);

		$conditions = " teacher_id = :teacher_id: ";
		$parameters = array(
			"teacher_id" =>$teacher_id
			);
		$teacher = OdpTeacher::findFirst(array($conditions,"bind"=>$parameters));
		$dep = $teacher->OdpDepartment;
		$place = $teacher->OdpNativePlace;
		
		$createProjects = CustomDetail::find(array(
				array("creator"=>array('$all'=>array(intval($teacher_id)))),
				'fields'=>array("type"=>1,"_id"=>0)
			));
		// print_r($createProjects);exit();
		$data = array(
			'teacher_id' =>$teacher->teacher_id, 
			'school_id' =>$teacher->school_id, 
			'department_id' =>$teacher->department_id, 
			'department_name'=>$dep->department_name,
			'staff_id' =>$teacher->staff_id, 
			'teacher_name' =>$teacher->teacher_name, 
			'professional_title' =>$teacher->professional_title, 
			'teacher_sex' =>$teacher->teacher_sex, 
			'is_inside_school' =>$teacher->is_inside_school, 
			'is_working' =>$teacher->is_working, 
			'nation_id' =>$teacher->nation_id, 
			'native_place_id' =>$teacher->native_place_id, 
			'native_place_name'=>$place->native_place_name,
			'start_work_time' =>$teacher->start_work_time, 
			'birthday' =>$teacher->birthday, 
			'email' =>$teacher->email, 
			'office_phone' =>$teacher->office_phone, 
			'mobile_phone' =>$teacher->mobile_phone, 
			'email_verificaion_code' =>$teacher->email_verificaion_code, 
			'is_email_verified' =>$teacher->is_email_verified, 
			'register_time' =>$teacher->register_time, 
			'last_login_time' =>$teacher->last_login_time, 
			'is_imported' =>$teacher->is_imported,
			'create_projects'=>$createProjects
			);
		$this->send_result($data);

	}


	public function customProjectAction($type)
	{
		$this->checkATK();

		$detail = CustomDetail::findfirst(array(
			array("type"=>$type),
			"fields"=>array("_id"=>0)
			));
		// echo "<pre>";
		// print_r($detail);
		// echo "</pre>";
		// echo "There are ", count($detail), "\n";
		$this->send_result($detail);
	}

	//不公开的API，获取所有的project的custom信息
	public function getCustomProjectsAction()
	{
		$this->checkATK();
		$t = new OdpTeacher();
		// var_dump($this->modelsMetadata->getDataTypes($t));exit();
		$detail = CustomDetail::find(array(
			"fields"=>array("_id"=>0)
			));
		$this->send_result($detail);
	}

	//不公开的API，获取所有特征字段
	public function getFeaturesAction()
	{
		$features = Features::find(array(
				"fields"=>array("_id"=>0)
			));
		$this->send_result($features);
	}

	public function getProjectFieldsAction($type)
	{
		// $this->checkATK();

		$detail = CustomDetail::findfirst(array(
			array("type"=>$type),
			"fields"=>array("_id"=>0)
			));
		var_dump($detail->fields);
	}

	public function getTeacherFeatureAction($teacher_id)
	{
		$teacherFeature = TeacherFeature::findfirst(array(
				"conditions" => array("tid"=>intval($teacher_id)),
				"fields"=>array("_id"=>0)
			));
		$this->send_result($teacherFeature);
	}

	public function getAllProjectsDetailAction()
	{
		$this->checkATK();
		//要全部详细的项目数据
		$projects =  QltyProject::find()->toArray();
		// var_dump($projects);exit();

		$this->send_result($projects);
	}

	public function getTeacherProjectsAction($teacher_id)
	{
		$this->checkATK();
		//要全部详细的项目数据
		$projects =  QltyProject::find("director_id = ".intval($teacher_id))->toArray();
		// var_dump($projects);exit();

		// $this->send_result("ds");
		$this->send_result($projects);
	}

	public function addProjectAction()
	{
		$this->checkATK();
		$project = new QltyProject();
		$param = $this->request->getPost('project');
		// $this->send_result($param);
		$project->assign(array(
					'director_id'=>intval($param['director_id']),
					'name'=>$param['name'],
					'type'=>$param['type'],
					'applyNO'=>$param['applyNO'],
					'apply_funds'=>intval($param['apply_funds']),
					'research_year_num'=>intval($param['research_year_num']),
					'status'=>intval($param['status'])
			));

		if($project->save()==FALSE)
		{	
			$this->send_result('fail');
			
		}else{
			$this->send_result('success');
		}
		
	}

	//以下两个需要补齐字段
	public function getProjectAction()
	{
		$this->checkATK();
		$params = $this->request->getPost('params');
		// $this->send_result($conditions);
		$conditions = "";
		//连接and
		$and = 0;
		if(isset($params['director_id']))
		{
			if($and) $conditions .=" AND ";
			$conditions .= "director_id = ".intval($params['director_id']);
			$and = 1;
		}
		if(isset($params['applyNO']))
		{
			if($and) $conditions .=" AND ";
			$conditions .="applyNO = '".$params['applyNO']."'";
			$and = 1;
		}

		// $this->send_result($conditions);
		$project = QltyProject::findfirst($conditions);
		// $this->send_result($project);
		if($project)
			$this->send_result($project);
		else
			$this->send_result('NOT_FOUND');
	}

	public function getProjectsAction()
	{
		$this->checkATK();
		$params = $this->request->getPost('params');
		// $this->send_result($conditions);
		$conditions = "";
		//连接and
		$and = 0;
		if(isset($params['director_id']))
		{
			if($and) $conditions .=" AND ";
			$conditions .= "director_id = ".intval($params['director_id']);
			$and = 1;
		}
		if(isset($params['applyNO']))
		{
			if($and) $conditions .=" AND ";
			$conditions .="applyNO = '".$params['applyNO']."'";
			$and = 1;
		}

		// $this->send_result($conditions);
		$project = QltyProject::find($conditions);
		// $this->send_result($project);
		if($project)
			$this->send_result($project->toArray());
		else
			$this->send_result('NOT_FOUND');
	}

/**********************************School**************************************/
	//获取青果部门数据
	/**
	 * @api {get} /school/departments 全部学校部门数据API
	 * @apiName Getdepartments
	 * @apiDescription 获取学校部门数据API
	 * @apiGroup School
	 * @apiVersion 0.1.0
	 *
	 * 
	 *
	 * @apiSampleRequest http://10.34.2.48/api/v1/school/departments
	 * @apiSuccess {String} Dept_id 部门ID
	 * @apiSuccess {String} Dept_name  部门名称
	 * @apiExample 调用示例:
	 *     curl -i http://10.34.2.48/api/v1/school/departments
	 * @apiExample 返回JSON示例:
	 * 
	 *{
     *       "Dept_id": "03",
     *       "Dept_name": "机电工程学院"
     * } 
	 */

	public function getDepartmentsAction()
	{
		$mssqlService = new MssqlService();
		$departments = $mssqlService->getDepartments();
		$this->send_result($departments);
	}

	
	

	//获取青果数据中的班级数据
	/**
	 * @api {get} /school/classes 全部班级数据API
	 * @apiName GetClasses
	 * @apiDescription 获取班级数据API
	 * @apiGroup School
	 * @apiVersion 0.1.0
	 *
	 * 
	 *
	 * @apiSampleRequest http://10.34.2.48/api/v1/school/classes
	 * @apiSuccess {String} Dept_id 部门ID
	 * @apiSuccess {String} Spec_id  专业ID
	 * @apiSuccess {String} class_id  班级ID
	 * @apiSuccess {String} class_name  班级名称
	 * @apiSuccess {String} myear  年级
	 * @apiSuccess {String} years  学制
	 * @apiExample 调用示例:
	 *     curl -i http://10.34.2.48/api/v1/school/classes
	 * @apiExample 返回JSON示例:
	 * 
 	 *{
     *       "Dept_id": "03",
     *       "Spec_id": "0307      ",
     *       "class_id": "2004030701",
     *       "class_name": "04机电",
     *       "myear": "2004",
     *       "years": "3"
     *}
	 */
	public function getClassesAction()
	{
		$mssqlService = new MssqlService();
		$classes = $mssqlService->getClasses();
		$this->send_result($classes);
	}

	//获取青果中的专业数据
	/**
	 * @api {get} /school/specialty 全部专业数据API
	 * @apiName Getspecialty
	 * @apiDescription 获取专业数据API
	 * @apiGroup School
	 * @apiVersion 0.1.0
	 *
	 * 
	 *
	 * @apiSampleRequest http://10.34.2.48/api/v1/school/specialty
	 * @apiSuccess {String} Dept_id 部门ID
	 * @apiSuccess {String} Spec_id  专业ID
	 * @apiSuccess {String} Spec_name  专业名称
	 * @apiSuccess {String} Guo_id  高职专业代码
	 * @apiExample 调用示例:
	 *     curl -i http://10.34.2.48/api/v1/school/specialty
	 * @apiExample 返回JSON示例:
	 * 
     *{
     *       "Dept_id": "04",
     *       "Spec_id": "0412      ",
     *       "Spec_name": "金融学(本科)",
     *       "Guo_id": "020104"
     *}
	 */
	public function getSpecialtyAction()
	{
		$mssqlService = new MssqlService();
		$spec = $mssqlService->getSpecialty();
		$this->send_result($spec);
	}


	/*******************************Teacher*****************************/
	/**
	 * @api {get} /teacher/all 全部教师数据API
	 * @apiName GetTeachers
	 * @apiDescription 获取教师数据API，注意此API受青果系统中数据不全影响
	 * @apiGroup Teacher
	 * @apiVersion 0.1.0
	 *
	 * 
	 *
	 * @apiSampleRequest http://10.34.2.48/api/v1/teacher/all
	 * @apiSuccess {String} teacher_id 教师青果工号
	 * @apiSuccess {String} teacher_name  教师姓名
	 * @apiSuccess {String} birthday  教师出生日期
	 * @apiSuccess {String} email  邮箱
	 * @apiSuccess {String} start_year  入校年份
	 * @apiSuccess {String} graduate_subject  毕业专业
	 * @apiSuccess {String} sex  性别
	 * @apiSuccess {String} dept_id  部门ID
	 * @apiSuccess {String} dept_name  部门名称
	 * @apiExample 调用示例:
	 *     curl -i http://10.34.2.48/api/v1/teacher/all
	 * @apiExample 返回JSON示例:
	 * 
     *{
     *       "teacher_id": "0000010",
     *       "teacher_name": "单××",
     *       "birthday": "19780701",
     *       "email": "shankefeng@cqdd.cq.cn",
     *       "start_year": "2000",
     *       "graduate_subject": "毕业于重庆理工大学会计学专业",
     *       "sex": "男",
     *       "dept_id": "22",
     *       "dept_name": "电子信息工程学院"
     *}
	 */
	public function getTeachersAction()
	{
		$mssqlService = new MssqlService();
		$teachers = $mssqlService->getTeachers();
		$this->send_result($teachers);
	}

	/**
	 * @api {get} /teacher/id/:teacher_id 按ID获取教师数据API
	 * @apiName GetTeacherByID
	 * @apiDescription 获取单个教师数据API，注意此API受青果系统中数据不全影响
	 * @apiGroup Teacher
	 * @apiVersion 0.1.0
	 *
	 * @apiParam {Number} teacher_id 青果教务系统工号 
	 *
	 * @apiSampleRequest http://10.34.2.48/api/v1/teacher/id/:teacher_id
	 * @apiSuccess {String} teacher_id 教师青果工号
	 * @apiSuccess {String} teacher_name  教师姓名
	 * @apiSuccess {String} birthday  教师出生日期
	 * @apiSuccess {String} email  邮箱
	 * @apiSuccess {String} start_year  入校年份
	 * @apiSuccess {String} graduate_subject  毕业专业
	 * @apiSuccess {String} sex  性别
	 * @apiSuccess {String} dept_id  部门ID
	 * @apiSuccess {String} dept_name  部门名称
	 * @apiSuccess {Array} projects  质量工程项目
	 * @apiExample 调用示例:
	 *     curl -i http://10.34.2.48/api/v1/teacher/id/0001692
	 * @apiExample 返回JSON示例:
	 * 
     *{
     *       "teacher_id": "0000010",
     *       "teacher_name": "单××",
     *       "birthday": "19780701",
     *       "email": "shankefeng@cqdd.cq.cn",
     *       "start_year": "2000",
     *       "graduate_subject": "毕业于重庆理工大学会计学专业",
     *       "sex": "男",
     *       "dept_id": "22",
     *       "dept_name": "电子信息工程学院"
     *}
	 */
	public function getTeacherByKSIdAction($teacher_id)
	{
		$mssqlService = new MssqlService();
		$teacher = $mssqlService->getTeacherByKSId($teacher_id);

		$conditions = " staff_id = :staff_id: ";
		$parameters = array(
			"staff_id" =>$teacher_id
			);
		$qlty_teacher = OdpTeacher::findfirst(array($conditions,"bind"=>$parameters));
		// var_dump($qlty_teacher);exit();
		if($qlty_teacher)
		{
			$projects = QltyProject::find(
				array("director_id = ".intval($qlty_teacher->teacher_id)))->toArray();
			if($projects)
			{
				$teacher['projects'] = $projects;
			}
		}

		$this->send_result($teacher);
		
	}
	/**
	 * @api {get} /teacher/department/:dept_id 按部门ID获取教师数据API
	 * @apiName GetTeacherByDeptID
	 * @apiDescription 按部门ID获取教师数据API，注意此API受青果系统中数据不全影响
	 * @apiGroup Teacher
	 * @apiVersion 0.1.0
	 *
	 * @apiParam {Number} dept_id 部门ID 
	 *
	 * @apiSampleRequest http://10.34.2.48/api/v1/teacher/department/:dept_id
	 * @apiSuccess {String} teacher_id 教师青果工号
	 * @apiSuccess {String} teacher_name  教师姓名
	 * @apiSuccess {String} birthday  教师出生日期
	 * @apiSuccess {String} email  邮箱
	 * @apiSuccess {String} start_year  入校年份
	 * @apiSuccess {String} graduate_subject  毕业专业
	 * @apiSuccess {String} sex  性别
	 * @apiSuccess {String} dept_id  部门ID
	 * @apiSuccess {String} dept_name  部门名称
	 * @apiExample 调用示例:
	 *     curl -i http://10.34.2.48/api/v1/teacher/department/22
	 * @apiExample 返回JSON示例:
	 * 
     *{
     *       "teacher_id": "0000010",
     *       "teacher_name": "单××",
     *       "birthday": "19780701",
     *       "email": "shankefeng@cqdd.cq.cn",
     *       "start_year": "2000",
     *       "graduate_subject": "毕业于重庆理工大学会计学专业",
     *       "sex": "男",
     *       "dept_id": "22",
     *       "dept_name": "电子信息工程学院"
     *}
	 */
	public function getTeachersByDeptIdAction($dept_id)
	{
		$mssqlService = new MssqlService();
		$teachers = $mssqlService->getTeachersByDeptId($dept_id);
		$this->send_result($teachers);
	}

	/********************************Student******************************************/

	/**
	 * @api {get} /student/class/:class_id 按班级ID获取学生数据API
	 * @apiName GetStudentsByClassId
	 * @apiDescription 按班级ID获取学生数据API
	 * @apiGroup Student
	 * @apiVersion 0.1.0
	 *
	 * @apiParam {Number} class_id 班级ID 
	 *
	 * @apiSampleRequest http://10.34.2.48/api/v1/student/class/:class_id
	 * @apiSuccess {String} user_xh 学号
	 * @apiSuccess {String} student_name  学生姓名
	 * @apiSuccess {String} sex  性别
	 * @apiSuccess {String} NJ  年级
	 * @apiSuccess {String} DM  院系代码
	 * @apiSuccess {String} user_dm  专业代码
	 * @apiSuccess {String} spec_name  专业名称
	 * @apiSuccess {String} GBZYDM  国标专业代码
	 * @apiSuccess {String} XZ  学制
	 * @apiSuccess {String} BJDM  班级代码
	 * @apiSuccess {String} class_name  班级名称
	 * @apiSuccess {String} xjzt  学籍状态
	 * @apiSuccess {String} sfzx  是否在校
	 * @apiSuccess {String} GKKSH  高考考生号
	 * @apiSuccess {String} GKZKZH  高考准考证号
	 * @apiSuccess {String} CSRQ  出生日期
	 * @apiSuccess {String} nation  民族
	 * @apiSuccess {String} politic  政治面貌
	 * @apiSuccess {String} RXCHJ  入学成绩
	 * @apiSuccess {String} RXSJ  入学时间
	 * @apiSuccess {String} dqmc  地区名称
	 * @apiSuccess {String} DQDM  地区代码
	 * @apiSuccess {String} SJR  联系人
	 * @apiSuccess {String} LXDH  联系电话
	 * @apiSuccess {String} YZBM  邮政编码
	 * @apiSuccess {String} email  电子邮箱
	 * @apiSuccess {String} in_specialty  录取专业
	 * @apiExample 调用示例:
	 *     curl -i http://10.34.2.48/api/v1//student/class/2014040202
	 * @apiExample 返回JSON示例:
	 * 
	 *{
     *       "user_xh": "1300577",
     *       "student_name": "岑袁",
     *       "sex": "男",
     *       "NJ": "2013",
     *       "DM": "04",
     *       "user_dm": "0404      ",
     *       "spec_name": "市场营销",
     *       "GBZYDM": "110202",
     *       "XZ": "3",
     *       "BJDM": "2013040404",
     *       "class_name": "13市场营销4班",
     *       "xjzt": "1 ",
     *       "sfzx": "0",
     *       "GKKSH": "13500130150195",
     *       "GKZKZH": "",
     *       "CSRQ": "Jul 12 1994 12:00:00:000AM",
     *       "nation": "汉族",
     *       "politic": "共青团员                      ",
     *       "RXCHJ": "287",
     *       "RXSJ": "Aug 18 2013 09:48:55:717AM",
     *       "dqmc": null,
     *       "DQDM": null,
     *       "SJR": "",
     *       "LXDH": "13628204949",
     *       "YZBM": "404343",
     *       "email": null,
     *       "in_specialty": "市场营销"
     *   }
	 */
	public function getStudentsByClassIdAction($class_id)
	{
		$mssqlService  = new MssqlService();
		$students = $mssqlService->getStudentsByClassId($class_id);
		$this->send_result($students);
	}

	/**
	 * @api {get} /student/id/:student_id 按学号获取学生数据API
	 * @apiName GetStudentById
	 * @apiDescription 按学号获取学生数据API
	 * @apiGroup Student
	 * @apiVersion 0.1.0
	 *
	 * @apiParam {Number} student_id 班级ID 
	 *
	 * @apiSampleRequest http://10.34.2.48/api/v1/student/id/:student_id
	 * @apiSuccess {String} user_xh 学号
	 * @apiSuccess {String} student_name  学生姓名
	 * @apiSuccess {String} sex  性别
	 * @apiSuccess {String} NJ  年级
	 * @apiSuccess {String} DM  院系代码
	 * @apiSuccess {String} user_dm  专业代码
	 * @apiSuccess {String} spec_name  专业名称
	 * @apiSuccess {String} GBZYDM  国标专业代码
	 * @apiSuccess {String} XZ  学制
	 * @apiSuccess {String} BJDM  班级代码
	 * @apiSuccess {String} class_name  班级名称
	 * @apiSuccess {String} xjzt  学籍状态
	 * @apiSuccess {String} sfzx  是否在校
	 * @apiSuccess {String} GKKSH  高考考生号
	 * @apiSuccess {String} GKZKZH  高考准考证号
	 * @apiSuccess {String} CSRQ  出生日期
	 * @apiSuccess {String} nation  民族
	 * @apiSuccess {String} politic  政治面貌
	 * @apiSuccess {String} RXCHJ  入学成绩
	 * @apiSuccess {String} RXSJ  入学时间
	 * @apiSuccess {String} dqmc  地区名称
	 * @apiSuccess {String} DQDM  地区代码
	 * @apiSuccess {String} SJR  联系人
	 * @apiSuccess {String} LXDH  联系电话
	 * @apiSuccess {String} YZBM  邮政编码
	 * @apiSuccess {String} email  电子邮箱
	 * @apiSuccess {String} in_specialty  录取专业
	 * @apiExample 调用示例:
	 *     curl -i http://10.34.2.48/api/v1//student/id/1400326
	 * @apiExample 返回JSON示例:
	 * 
	 *{
     *       "user_xh": "1300577",
     *       "student_name": "岑袁",
     *       "sex": "男",
     *       "NJ": "2013",
     *       "DM": "04",
     *       "user_dm": "0404      ",
     *       "spec_name": "市场营销",
     *       "GBZYDM": "110202",
     *       "XZ": "3",
     *       "BJDM": "2013040404",
     *       "class_name": "13市场营销4班",
     *       "xjzt": "1 ",
     *       "sfzx": "0",
     *       "GKKSH": "13500130150195",
     *       "GKZKZH": "",
     *       "CSRQ": "Jul 12 1994 12:00:00:000AM",
     *       "nation": "汉族",
     *       "politic": "共青团员                      ",
     *       "RXCHJ": "287",
     *       "RXSJ": "Aug 18 2013 09:48:55:717AM",
     *       "dqmc": null,
     *       "DQDM": null,
     *       "SJR": "",
     *       "LXDH": "13628204949",
     *       "YZBM": "404343",
     *       "email": null,
     *       "in_specialty": "市场营销"
     *   }
	 */
	public function getStudentByIdAction($student_id)
	{
		$mssqlService  = new MssqlService();
		$students = $mssqlService->getStudentById($student_id);
		$this->send_result($students);
	}


	/*********************************Score***************************************/
	/**
	 * @api {get} /score/course/:course_id 按课程ID获取成绩数据API
	 * @apiName GetScoreByCourseID
	 * @apiDescription 按课程ID获取成绩数据API
	 * @apiGroup Score
	 * @apiVersion 0.1.0
	 *
	 * @apiParam {Number} course_id 课程代码 
	 *
	 * @apiSampleRequest http://10.34.2.48/api/v1/score/course/:course_id
	 * @apiSuccess {String} student_id 学号
	 * @apiSuccess {String} student_name  学生姓名
	 * @apiSuccess {String} dept_name  院系
	 * @apiSuccess {String} class_name  班级名称
	 * @apiSuccess {String} spec_name  专业名称
	 * @apiSuccess {String} KCDM  课程代码
	 * @apiSuccess {String} course_name  课程名称
	 * @apiSuccess {String} PSCJ  平时成绩
	 * @apiSuccess {String} QMCJ  期末成绩
	 * @apiSuccess {String} KSCJ  总成绩
	 * @apiExample 调用示例:
	 *     curl -i http://10.34.2.48/api/v1/score/course/030456
	 * @apiExample 返回JSON示例:
	 * 
     *   {
     *       "student_id": "0404360     ",
     *       "student_name": "朱恒",
     *       "dept_name": "电子信息工程学院",
     *       "class_name": "04通信",
     *       "spec_name": "通信工程",
     *       "KCDM": "030456",
     *       "course_name": "模拟电子电路",
     *       "PSCJ": "88.00",
     *       "QMCJ": "85.00",
     *       "KSCJ": "86.00"
     *   }
	 */

	public function getScoreByCourseIdAction($course_id)
	{
		$mssqlService  = new MssqlService();
		$students = $mssqlService->getScoreByCourseId($course_id);
		$this->send_result($students);
	}

	/**
	 * @api {get} /score/student/:student_id 按学号获取成绩数据API
	 * @apiName GetScoreByStudentID
	 * @apiDescription 按学号获取成绩数据API
	 * @apiGroup Score
	 * @apiVersion 0.1.0
	 *
	 * @apiParam {Number} student_id 学号 
	 *
	 * @apiSampleRequest http://10.34.2.48/api/v1/score/student/:student_id
	 * @apiSuccess {String} student_id 学号
	 * @apiSuccess {String} student_name  学生姓名
	 * @apiSuccess {String} dept_name  院系
	 * @apiSuccess {String} class_name  班级名称
	 * @apiSuccess {String} spec_name  专业名称
	 * @apiSuccess {String} KCDM  课程代码
	 * @apiSuccess {String} course_name  课程名称
	 * @apiSuccess {String} PSCJ  平时成绩
	 * @apiSuccess {String} QMCJ  期末成绩
	 * @apiSuccess {String} KSCJ  总成绩
	 * @apiExample 调用示例:
	 *     curl -i http://10.34.2.48/api/v1/score/student/0404369
	 * @apiExample 返回JSON示例:
	 * 
     *   {
     *       "student_id": "0404360",
     *       "student_name": "朱恒",
     *       "dept_name": "电子信息工程学院",
     *       "class_name": "04通信",
     *       "spec_name": "通信工程",
     *       "KCDM": "030456",
     *       "course_name": "模拟电子电路",
     *       "PSCJ": "88.00",
     *       "QMCJ": "85.00",
     *       "KSCJ": "86.00"
     *   }
	 */
	public function getScoreByStudentIdASction($student_id)
	{
		$mssqlService  = new MssqlService();
		$students = $mssqlService->getScoreByStudentId($student_id);
		$this->send_result($students);
	}

	/*******************************Course********************************/
	/**
	 * @api {get} /course/specialty/:spec_id 按专业获取课程数据API
	 * @apiName GetCourseBySpecId
	 * @apiDescription 按专业获取课程数据API
	 * @apiGroup Course
	 * @apiVersion 0.1.0
	 *
	 * @apiParam {Number} spec_id 课程代码 
	 *
	 * @apiSampleRequest http://10.34.2.48/api/v1/course/specialty/:spec_id
	 * @apiSuccess {String} XN 学年
	 * @apiSuccess {String} XQ_ID  学期
	 * @apiSuccess {String} user_kcdm  课程代码
	 * @apiSuccess {String} kcmc  课程名称
	 * @apiSuccess {String} zjjs_gh  主讲教师工号
	 * @apiSuccess {String} zjjs_xm  主讲教师姓名
	 * @apiSuccess {String} user_bjdm  班级代码
	 * @apiSuccess {String} class_name  班级名称
	 * @apiSuccess {String} stimezc  上课周次
	 * @apiSuccess {String} JCInfo  节次
	 * @apiSuccess {String} room_dm  教室代码
	 * @apiSuccess {String} room  教室名称
	 * @apiSuccess {String} SKAP 上课安排
	 * @apiSuccess {String} KHFS 考核方式
	 * @apiSuccess {String} xf 学分
	 * @apiSuccess {String} jsxs 讲授学时
	 * @apiSuccess {String} NJ 年级
	 * @apiSuccess {String} YXMC 院系名称
	 * @apiSuccess {String} ZYDM_USER 专业代码
	 * @apiSuccess {String} ZYMC 专业名称
	 * @apiSuccess {String} kclb1mc 课程类别
	 * @apiSuccess {String} zongxs 总学时
	 * @apiSuccess {String} sjxs 实践学时
	 * @apiSuccess {String} stu_sum 上课总人数
	 * @apiExample 调用示例:
	 *     curl -i http://10.34.2.48/api/v1/course/specialty/2232
	 * @apiExample 返回JSON示例:
	 * 
     *{
     *       "XN": "2011",
     *       "XQ_ID": "0",
     *       "user_kcdm": "031088    ",
     *       "kcmc": "计算机文化",
     *       "zjjs_gh": "0002234",
     *       "zjjs_xm": "范兴亮",
     *       "user_bjdm": "2011223201",
     *       "class_name": "11软件技术2班",
     *       "stimezc": "10-16,18",
     *       "JCInfo": "7-8",
     *       "room_dm": "2033307             ",
     *       "room": "修德楼C307",
     *       "SKAP": "[10-16,18周]二[7-8节]",
     *       "KHFS": "期考      ",
     *       "xf": "4.0",
     *       "jsxs": "24.0",
     *       "NJ": "2011",
     *       "YXMC": "电子信息工程学院",
     *       "ZYDM_USER": "2232      ",
     *       "ZYMC": "软件技术(应用软件开发)",
     *       "kclb1mc": "必修课",
     *       "zongxs": "64.0",
     *       "sjxs": "40",
     *       "stu_sum": "75"
     *}
	 */
	public function getCourseBySpecIdAction($spec_id)
	{
		$mssqlService  = new MssqlService();
		$students = $mssqlService->getCourseBySpecId($spec_id);
		$this->send_result($students);
	}

	/**
	 * @api {get} /course/class/:class_id 按班级获取课程数据API
	 * @apiName GetCourseByClassId
	 * @apiDescription 按班级获取课程数据API
	 * @apiGroup Course
	 * @apiVersion 0.1.0
	 *
	 * @apiParam {Number} class_id 课程代码 
	 *
	 * @apiSampleRequest http://10.34.2.48/api/v1/course/class/:class_id
	 * @apiSuccess {String} XN 学年
	 * @apiSuccess {String} XQ_ID  学期
	 * @apiSuccess {String} user_kcdm  课程代码
	 * @apiSuccess {String} kcmc  课程名称
	 * @apiSuccess {String} zjjs_gh  主讲教师工号
	 * @apiSuccess {String} zjjs_xm  主讲教师姓名
	 * @apiSuccess {String} user_bjdm  班级代码
	 * @apiSuccess {String} class_name  班级名称
	 * @apiSuccess {String} stimezc  上课周次
	 * @apiSuccess {String} JCInfo  节次
	 * @apiSuccess {String} room_dm  教室代码
	 * @apiSuccess {String} room  教室名称
	 * @apiSuccess {String} SKAP 上课安排
	 * @apiSuccess {String} KHFS 考核方式
	 * @apiSuccess {String} xf 学分
	 * @apiSuccess {String} jsxs 讲授学时
	 * @apiSuccess {String} NJ 年级
	 * @apiSuccess {String} YXMC 院系名称
	 * @apiSuccess {String} ZYDM_USER 专业代码
	 * @apiSuccess {String} ZYMC 专业名称
	 * @apiSuccess {String} kclb1mc 课程类别
	 * @apiSuccess {String} zongxs 总学时
	 * @apiSuccess {String} sjxs 实践学时
	 * @apiSuccess {String} stu_sum 上课总人数
	 * @apiExample 调用示例:
	 *     curl -i http://10.34.2.48/api/v1/course/class/2014040202
	 * @apiExample 返回JSON示例:
	 * 
     *{
     *       "XN": "2011",
     *       "XQ_ID": "0",
     *       "user_kcdm": "031088    ",
     *       "kcmc": "计算机文化",
     *       "zjjs_gh": "0002234",
     *       "zjjs_xm": "范兴亮",
     *       "user_bjdm": "2011223201",
     *       "class_name": "11软件技术2班",
     *       "stimezc": "10-16,18",
     *       "JCInfo": "7-8",
     *       "room_dm": "2033307             ",
     *       "room": "修德楼C307",
     *       "SKAP": "[10-16,18周]二[7-8节]",
     *       "KHFS": "期考      ",
     *       "xf": "4.0",
     *       "jsxs": "24.0",
     *       "NJ": "2011",
     *       "YXMC": "电子信息工程学院",
     *       "ZYDM_USER": "2232      ",
     *       "ZYMC": "软件技术(应用软件开发)",
     *       "kclb1mc": "必修课",
     *       "zongxs": "64.0",
     *       "sjxs": "40",
     *       "stu_sum": "75"
     *}
	 */
	public function getCourseByClassIdAction($class_id)
	{
		$mssqlService  = new MssqlService();
		$students = $mssqlService->getCourseByClassId($class_id);
		$this->send_result($students);
	}
}