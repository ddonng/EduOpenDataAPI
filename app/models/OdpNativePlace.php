<?php

class OdpNativePlace extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $native_place_id;

    /**
     *
     * @var string
     */
    public $native_place_name;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'native_place_id' => 'native_place_id', 
            'native_place_name' => 'native_place_name'
        );
    }
    
    public function initialize()
    {
        $this->hasMany('native_place_id','OdpTeacher','native_place_id');
    }

}
