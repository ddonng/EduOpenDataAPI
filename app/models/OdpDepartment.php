<?php

class OdpDepartment extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $department_id;

    /**
     *
     * @var integer
     */
    public $is_inside_school;

    /**
     *
     * @var string
     */
    public $department_name;

    public function initialize()
    {
        $this->hasMany('department_id','OdpTeacher','department_id');
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'department_id' => 'department_id', 
            'is_inside_school' => 'is_inside_school', 
            'department_name' => 'department_name'
        );
    }

}
