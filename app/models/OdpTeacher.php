<?php

use Phalcon\Mvc\Model\Validator\Email as Email;

class OdpTeacher extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $teacher_id;

    /**
     *
     * @var string
     */
    public $school_id;

    /**
     *
     * @var integer
     */
    public $department_id;

    /**
     *
     * @var string
     */
    public $staff_id;

    /**
     *
     * @var string
     */
    public $teacher_name;

    /**
     *
     * @var string
     */
    public $professional_title;

    /**
     *
     * @var string
     */
    public $teacher_pwd;

    /**
     *
     * @var integer
     */
    public $teacher_sex;

    /**
     *
     * @var integer
     */
    public $is_inside_school;

    /**
     *
     * @var integer
     */
    public $is_working;

    /**
     *
     * @var integer
     */
    public $nation_id;

    /**
     *
     * @var integer
     */
    public $native_place_id;

    /**
     *
     * @var integer
     */
    public $start_work_time;

    /**
     *
     * @var string
     */
    public $birthday;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $office_phone;

    /**
     *
     * @var string
     */
    public $mobile_phone;

    /**
     *
     * @var string
     */
    public $email_verificaion_code;

    /**
     *
     * @var integer
     */
    public $is_email_verified;

    /**
     *
     * @var integer
     */
    public $register_time;

    /**
     *
     * @var integer
     */
    public $last_login_time;

    /**
     *
     * @var integer
     */
    public $is_imported;

    /**
     *
     * @var string
     */
    public $duty;
    
    //将odp_department表关联进来
    public function initialize()
    {
        $this->belongsTo('department_id','OdpDepartment','department_id');
        $this->belongsTo('native_place_id','OdpNativePlace','native_place_id');
        $this->belongsTo('teacher_id','QltyProject','director_id');
    }

    /**
     * Validations and business logic
     */
    public function validation()
    {

        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'teacher_id' => 'teacher_id', 
            'school_id' => 'school_id', 
            'department_id' => 'department_id', 
            'staff_id' => 'staff_id', 
            'teacher_name' => 'teacher_name', 
            'professional_title' => 'professional_title', 
            'teacher_pwd' => 'teacher_pwd', 
            'teacher_sex' => 'teacher_sex', 
            'is_inside_school' => 'is_inside_school', 
            'is_working' => 'is_working', 
            'nation_id' => 'nation_id', 
            'native_place_id' => 'native_place_id', 
            'start_work_time' => 'start_work_time', 
            'birthday' => 'birthday', 
            'email' => 'email', 
            'office_phone' => 'office_phone', 
            'mobile_phone' => 'mobile_phone', 
            'email_verificaion_code' => 'email_verificaion_code', 
            'is_email_verified' => 'is_email_verified', 
            'register_time' => 'register_time', 
            'last_login_time' => 'last_login_time', 
            'is_imported' => 'is_imported', 
            'duty' => 'duty'
        );
    }

}
