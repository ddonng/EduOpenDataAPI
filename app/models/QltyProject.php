<?php

class QltyProject extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $qlty_project_id;

    /**
     *
     * @var integer
     */
    public $director_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $type;

    /**
     *
     * @var integer
     */
    public $category;

    /**
     *
     * @var integer
     */
    public $rank;

    /**
     *
     * @var integer
     */
    public $belong_cqdd;

    /**
     *
     * @var integer
     */
    public $belong_specialty;

    /**
     *
     * @var double
     */
    public $apply_funds;

    /**
     *
     * @var integer
     */
    public $research_year_num;

    /**
     *
     * @var string
     */
    public $achievement;

    /**
     *
     * @var string
     */
    public $course_url;

    /**
     *
     * @var integer
     */
    public $is_backbone;

    /**
     *
     * @var integer
     */
    public $sessions;

    /**
     *
     * @var string
     */
    public $course_teach_experience;

    /**
     *
     * @var integer
     */
    public $created_time;

    /**
     *
     * @var integer
     */
    public $apply_time;

    /**
     *
     * @var integer
     */
    public $established_time;

    /**
     *
     * @var integer
     */
    public $change_time;

    /**
     *
     * @var integer
     */
    public $middle_time;

    /**
     *
     * @var integer
     */
    public $done_time;

    /**
     *
     * @var string
     */
    public $NO;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     *
     * @var integer
     */
    public $is_imported;

    /**
     *
     * @var integer
     */
    public $fund_finished;

    public function initialize()
    {
        $this->belongsTo('director_id','OdpTeacher','teacher_id');
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'qlty_project_id' => 'qlty_project_id', 
            'director_id' => 'director_id', 
            'name' => 'name', 
            'type' => 'type', 
            'category' => 'category', 
            'rank' => 'rank', 
            'belong_cqdd' => 'belong_cqdd', 
            'belong_specialty' => 'belong_specialty', 
            'apply_funds' => 'apply_funds', 
            'research_year_num' => 'research_year_num', 
            'achievement' => 'achievement', 
            'course_url' => 'course_url', 
            'is_backbone' => 'is_backbone', 
            'sessions' => 'sessions', 
            'course_teach_experience' => 'course_teach_experience', 
            'created_time' => 'created_time', 
            'apply_time' => 'apply_time', 
            'established_time' => 'established_time', 
            'change_time' => 'change_time', 
            'middle_time' => 'middle_time', 
            'done_time' => 'done_time', 
            'NO' => 'NO', 
            'status' => 'status', 
            'is_imported' => 'is_imported', 
            'fund_finished' => 'fund_finished'
        );
    }

}
