<?php

Class MssqlService
{
	private static $db;

	public function __construct()
	{
		if(is_null(self::$db) || !isset(self::$db))
		{
			try {
				$hostname = "";
				$dbname = "";
				$username = "";
				$pw = "";
				$db = new PDO ("dblib:host=$hostname;dbname=$dbname;charset=utf8","$username","$pw");

				self::$db = $db;
			} catch (PDOException $e) {
				echo "Failed to get DB handle: " . $e->getMessage() . "\n";
				exit;
			}
		}
	}

	private function mult_iconv($data,$in_charset='GB2312',$out_charset="utf-8//IGNORE")
	{
		
		
		if(substr($out_charset,-8)=='//IGNORE'){
			$out_charset=substr($out_charset,0,-8);
		}
		if(is_array($data)){
			foreach($data as $key => $value){
				if(is_array($value)){
					$key=iconv($in_charset,$out_charset.'//IGNORE',$key);
					$rtn[$key]=$this->mult_iconv($value,$in_charset,$out_charset);
				}elseif(is_string($key) || is_string($value)){
					if(is_string($key)){
						$key=iconv($in_charset,$out_charset.'//IGNORE',$key);
					}
					if(is_string($value)){
						$value=iconv($in_charset,$out_charset.'//IGNORE',$value);
					}
					$rtn[$key]=$value;
				}else{
					$rtn[$key]=$value;
				}
			}
		}elseif(is_string($data)){
			$rtn=iconv($in_charset,$out_charset.'//IGNORE',$data);
		}else{
			$rtn=$data;
		}
		return $rtn;
	}
	//从青果中获取部门数据

	public function getDepartments()
	{
		  $stmt = self::$db->prepare("select * from cqtbi_dept");
		  $stmt->execute();

		  $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		  // iconv('GBK', 'UTF-8//IGNORE', $results);
		  // $data =eval('return '.iconv('gbk','utf-8',var_export($results,true)).';');
		  // echo mb_detect_encoding($results[0]['class_name'], "GB2312,GBK,UTF-8,ASCII,EUC-CN,CP936,BIG-5");
		  // return iconv('cp936','UTF-8//IGNORE//TRANSLIT','12¾Æµê1°à');
		  // return mb_convert_encoding($results[0]['class_name'],'UTF-8','cp936');
		  return $results;
	}

	//获取班级数据
	public function getClasses()
	{
		$stmt = self::$db->prepare("SELECT * FROM cqtbi_class");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	//获取专业数据
	public function getSpecialty()
	{
		$stmt = self::$db->prepare("SELECT * FROM cqtbi_spec");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	//获取全部教师数据
	public function getTeachers()
	{
		$stmt = self::$db->prepare("SELECT * FROM cqtbi_teacher");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	//青果工号获取某教师信息
	public function getTeacherByKSId($teacher_id)
	{
		$stmt = self::$db->prepare("SELECT * FROM cqtbi_teacher WHERE teacher_id=:teacher_id");
		$stmt->execute(array(':teacher_id'=>$teacher_id));
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	//按部门ID获取教师数据
	public function getTeachersByDeptId($dept_id)
	{
		$stmt = self::$db->prepare("SELECT * FROM cqtbi_teacher WHERE dept_id=:dept_id");
		$stmt->execute(array(':dept_id'=>$dept_id));
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	//按班级代码获取学生数据
	public function getStudentsByClassId($class_id)
	{
		$stmt = self::$db->prepare("SELECT * FROM cqtbi_student WHERE [BJDM]=:class_id");
		$stmt->execute(array(':class_id'=>$class_id));
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	//按学号获取学生数据
	public function getStudentById($student_id)
	{
		$stmt = self::$db->prepare("SELECT * FROM cqtbi_student WHERE [user_xh]=:student_id");
		$stmt->execute(array(':student_id'=>$student_id));
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	//按课程获取成绩数据
	public function getScoreByCourseId($course_id)
	{
		$stmt = self::$db->prepare("SELECT * FROM cqtbi_score WHERE [KCDM]=:course_id");
		$stmt->execute(array(':course_id'=>$course_id));
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	//按学号获取成绩数据
	public function getScoreByStudentId($student_id)
	{
		$stmt = self::$db->prepare("SELECT * FROM cqtbi_score WHERE [student_id]=:student_id");
		$stmt->execute(array(':student_id'=>$student_id));
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	//按专业获取课程数据
	public function getCourseBySpecId($spec_id)
	{
		$stmt = self::$db->prepare("SELECT * FROM cqtbi_courses WHERE [ZYDM_USER]=:spec_id");
		$stmt->execute(array(':spec_id'=>$spec_id));
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}


	//按班级获取课程数据
	public function getCourseByClassId($class_id)
	{
		$stmt = self::$db->prepare("SELECT * FROM cqtbi_courses WHERE [user_bjdm]=:class_id");
		$stmt->execute(array(':class_id'=>$class_id));
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}
}