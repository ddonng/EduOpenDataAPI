<?php
require_once('OAuth2/Autoloader.php');

class OauthServer
{
	private static $server=NULL; 

	private $dsn;

	private $username;

	private $password;

	public function __construct()
	{
		$this->dsn = "mysql:host=localhost;dbname=oauth";
		$this->username='root';
		$this->password='CqtbiD204';
		OAuth2\Autoloader::register();
	}

	public function getServerInstance()
	{
	        if (is_null(self::$server) || !isset(self::$server)) {
				$storage = new OAuth2\Storage\Pdo(new PDO('mysql:host=localhost;dbname=oauth', $this->username, $this->password));

				$server = new OAuth2\Server($storage);

				$server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));

				$server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));

				$server->addGrantType(new OAuth2\GrantType\RefreshToken($storage));

				// configure your available scopes
				$defaultScope = 'basic';
				$supportedScopes = array(
				  'basic',
				  'pinfo'
				);
				$memory = new OAuth2\Storage\Memory(array(
				  'default_scope' => $defaultScope,
				  'supported_scopes' => $supportedScopes
				));
				$scopeUtil = new OAuth2\Scope($memory);

				$server->setScopeUtil($scopeUtil);

	            self::$server = $server;
	        }
	        return self::$server;
	}


}